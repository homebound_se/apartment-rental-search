#!/usr/bin/env python3

import csv
import os
from blocket import blocket_fetch_all
from bostaddirekt import bostaddirekt_fetch_all
from hemnet import hemnet_fetch_all
from qasa import qasa_fetch_all

APARTMENTS_CSV = 'apartments.csv'


def acceptable_apartment_rent(entry):
    """
    Check if the apartment is acceptable for rent.
    """

    try:
        if float(entry['rent']) > 11000.0:
            return False
    except ValueError:
        pass

    return True


def acceptable_apartment_buy(entry):
    """
    Check if the apartment is acceptable to buy
    """

    try:
        if float(entry['price']) > 2000000.0:
            return False
    except ValueError:
        pass

    return True


def main():
    """
    Application entry point
    """
    if os.path.isfile(APARTMENTS_CSV):
        os.remove(APARTMENTS_CSV)

    exclusions = [
        'Botkyrka', 'Ekerö', 'Haninge', 'Huddinge', 'Järfälla', 'Lidingö',
        'Nacka','Norrtälje', 'Nykvarn', 'Nynäshamn', 'Salem', 'Sigtuna',
        'Södertälje', 'Tyresö', 'Täby', 'Upplands Väsby', 'Upplands-Bro',
        'Vallentuna','Vaxholm', 'Värmdö', 'Österåker'
    ]

    fieldnames = [
        'id', 'pub_time', 'rent', 'rooms', 'size', 'location',
        'heading', 'url', 'price', 'avgift', 'area'
    ]

    csvfile = open(APARTMENTS_CSV, 'w')
    csvw = csv.DictWriter(csvfile, fieldnames=fieldnames)
    csvw.writeheader()

    # qasa_fetch_all(exclusions)

    # Blocket fetch will yield for an Apartment object
    for item in blocket_fetch_all(exclusions, page_limit=10, save_pages=False):
        if not acceptable_apartment_rent(item):
            continue

        print('Blocket item:', item)
        csvw.writerow(item)

    exclusions.append('Bagarmossen')
    exclusions.append('Bandhagen')
    exclusions.append('Hägersten')

    # Bostad Direkt
    for item in bostaddirekt_fetch_all(exclusions):
        if not acceptable_apartment_rent(item):
           continue

        print('BostadDirekt item:', item)
        csvw.writerow(item)

    # Hemnet
    # <Stockholms län>:
    # 18031 = Stockholm kommun
    # 18028 = Solna kommun
    # 18027 = Sollentuna
    # 17892 = Danderyd kommun
    # 18042 = Sundbyberg kommun
    # 17793 = Taby kommun
    # 898748 = Kungsholmen
    # 898747 = Kista, Hasselby, Vallingby, Spanga
    # 898740 = Bromma
    # 17853 = Nacka kommun
    # 17798 = Upplands Vasby
    # 17744 = ALL kommun in Stockholms län!

    location_id = '898747'
    for item in hemnet_fetch_all(location_id):
        if not acceptable_apartment_buy(item):
            continue
        print('Hemnet item:', item)
        csvw.writerow(item)

    csvfile.close()


if __name__ == '__main__':
    main()
