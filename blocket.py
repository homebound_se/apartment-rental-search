"""
Web scraper for https://www.blocket.se/bostad/uthyres/stockholm?f=p&f=c&f=b

Example URL: https://www.blocket.se/bostad/uthyres/stockholm?sort=&f=p&f=c&f=b&m=116

"""

import os
import random
import re
import time
import mechanicalsoup


BLOCKET_RENTALS = 'https://www.blocket.se/bostad/uthyres/stockholm?f=p&f=c&f=b'
TARGET_DIR = 'data'


def ensure_dir(directory):
    """
    Check if the given directory exists and create a directory if it doesn't.
    """
    if not os.path.exists('%s/%s' % (TARGET_DIR, directory)):
        os.makedirs('%s/%s' % (TARGET_DIR, directory))


def blocket_fetch_items(page, save_pages):
    """
    Handle one page of Blocket rental data.
    """
    items = page.find('div', id='item_list')

    for item in items:
        if type(item).__name__ != 'Tag':
            continue

        item_id = item['id']
        url = item.a['href']
        item_body = item.find('div', class_='media-body')
        heading = item_body.find('h4', class_='media-heading') \
                           .a.text.strip()
        location = item_body.find('div', class_='item-location') \
                            .find('span', class_='address').text.strip()
        details = item_body.find('div', class_='details')
        list_time = item_body.find('time')['datetime']
        rooms = 'Unspecified'
        rent = 'Unspecified'
        size = 'Unspecified'
        if type(details).__name__ != 'NoneType':
            tag = details.find('span', class_='rooms')
            rooms = tag.text.strip() if tag is not None else 'Unspecified'
            tag = details.find('span', class_='monthly_rent')
            rent = re.sub('[^0-9]', '', tag.text.strip()) if tag is not None \
                                                          else 'Unspecified'
            tag = details.find('span', class_='size')
            size = tag.text.strip() if tag is not None else 'Unspecified'

        # Download and save data
        filename = '%s/blocket/blocket_%s.html' % (TARGET_DIR, item_id)
        if save_pages and not os.path.isfile(filename):
            browser = mechanicalsoup.StatefulBrowser()
            browser.open(url)
            page = browser.get_current_page()

            print('Save Path:', filename)
            with open(filename, 'wb') as f:
                f.write(str.encode(str(page)))

            time.sleep(random.randint(3, 5))

        yield {
            'id': item_id,
            'heading': heading,
            'location': location,
            'pub_time': list_time,
            'rent': rent,
            'rooms': rooms,
            'size': size,
            'url': url
        }


def blocket_fetch_all(exclusions, page_limit=1, save_pages=True):
    """
    Fetch listings from blocket.se
    """
    print('Searching from Blocket.se')

    browser = mechanicalsoup.StatefulBrowser()
    # Uncomment for a more verbose output:
    # browser.set_verbose(2)

    browser.open(BLOCKET_RENTALS)

    page = browser.get_current_page()
    areas = page.find('select', id='searcharea_region_multiselect')
    area_query = '&'.join([
        ('as=' if 'title' in area.attrs else 'm=') + area['value']
        for area in areas.findAll('option') if not area.string in exclusions
    ])

    # Iterate through all the pages available in Blocket
    browser.open('&'.join([BLOCKET_RENTALS, area_query]))  # Initial page
    page = browser.get_current_page()
    next_page = page.find('ul', id='pagination').find('li', class_='next')

    while next_page is not None:
        # Handle one page of data here
        active = page.find('ul', id='pagination').find('li', class_='active').a.text
        print('Processing Page:', active)

        for item in blocket_fetch_items(page, save_pages):
            yield item

        if int(active) == page_limit:
            break

        time.sleep(random.randint(3, 10))  # Random sleep

        browser.open(next_page.a['href'])
        page = browser.get_current_page()
        next_page = page.find('ul', id='pagination').find('li', class_='next')
