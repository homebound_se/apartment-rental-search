"""
Web scraper for Bostad Direkt

"""

import json
import re
import random
import time
import token
import tokenize
from io import StringIO

import mechanicalsoup


BOSTAD_RENTALS = 'http://www.bostaddirekt.com/JsonEstates.aspx?custType=0&RoomsMin=0&SizeMin=0&RentMax=50000&PeriodMinMax=min&Period=1&Apmnt=1&Other=1&Room=1&Furnished=-1&MoveInDate=&Areas%5B%5D=11e0d3075593bf3fd65dfd41de5a1323aa359255%3AStockholms%20l%C3%A4n&Sort=&Page='


def fix_lazy_json(in_text):
    '''
    Normalize JSON string coming from the server.
    '''
    tokengen = tokenize.generate_tokens(StringIO(in_text).readline)

    result = []
    for tokid, tokval, _, _, _ in tokengen:
        # fix unquoted strings
        if tokid == token.NAME:
            if tokval not in ['true', 'false', 'null', '-Infinity', 'Infinity', 'NaN']:
                tokid = token.STRING
                tokval = u'"%s"' % tokval

        # fix single-quoted strings
        elif tokid == token.STRING:
            if tokval.startswith ("'"):
                tokval = u'"%s"' % tokval[1:-1].replace('"', '\\"')

        # remove invalid commas
        elif tokid == token.OP and (tokval == '}' or tokval == ']'):
            if len(result) > 0 and result[-1][1] == ',':
                result.pop()

        # fix single-quoted strings
        elif tokid == token.STRING:
            if tokval.startswith("'"):
                tokval = u'"%s"' % tokval[1:-1].replace('"', '\\"')

        result.append((tokid, tokval))

    return tokenize.untokenize(result)


def bostaddirekt_fetch_all(exclusions, page_limit=1, save_pages=True):
    """
    Fetch for rent residences from bostaddirekt.se
    """
    done = False
    page_number = 1
    estates_found = 0
    print('Searching from BostadDirekt.se')

    while not done:
        print('Processing Page ' + str(page_number))
        browser = mechanicalsoup.StatefulBrowser()
        browser.open(BOSTAD_RENTALS + str(page_number))

        page = browser.get_current_page()
        data = re.sub('(?<=description:).*?(\',)', '\'\',', page.find('p').text)
        data = re.sub('(?<=title:).*?(\'})', '\'\'}', data)
        data = fix_lazy_json(re.sub('(?<=coords:).*?(\))', '\'\'', data))
        items = json.loads(data)
        total = len(items)

        if total == 0:
            break

        estates_found = estates_found + total

        for item in items:
            if not done and estates_found == item['estatesFound']:
                done = True

            yield {
                'id': item['id'],
                'heading': item['shareTitle'],
                'location': item['area'] if item['area'] != '' else item['address'],
                'pub_time': 'TODO',
                'rent': re.sub('[^0-9]', '', item['rent']),
                'rooms': str(item['rooms']) + ' rum',
                'size': item['size'],
                'url': 'http://www.bostaddirekt.com/Private/estate.aspx?id=' + str(item['id'])
            }

        page_number = page_number + 1
        time.sleep(random.randint(3, 10))
