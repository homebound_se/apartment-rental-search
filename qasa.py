"""
Web scraper https://en.qasa.se/rent-home/sweden
"""

import mechanicalsoup


QASA_RENTALS = 'https://en.qasa.se/rent-home/sweden'


def qasa_fetch_all(exclusions, page_limit=1, save_pages=True):
    """
    Fetch all apartment listings from Qasa.se.
    """
    print('Searching from Blocket.se')
    browser = mechanicalsoup.StatefulBrowser()

    browser.open(QASA_RENTALS)

    page = browser.get_current_page()
    items = page.find('div', class_="list-content-wrapper").find('ul')

    for item in items:
        print('Item:', item)
