#!/usr/bin/env python3
"""
Web scraper for https://www.hemnet.se/bostader?item_types%5B%5D=bostadsratt

"""

import os
import random
import re
import time
import mechanicalsoup

HEMNET_BOSTAD_SELL_URL  = 'https://www.hemnet.se/bostader?item_types%5B%5D=bostadsratt'
SAMPLE_SEARCH_URL       = 'https://www.hemnet.se/bostader?region_id=17744&item_types%5B%5D=bostadsratt'

def hemnet_fetch_items(page, save_pages):
    """
    Find all the available items from this page.
    """

    items = page.find('ul', id='search-results') \
                .findAll("li", { "class" : "results__normal-item" })

    for item in items:
        if type(item).__name__ != 'Tag':
            continue

        price = ''
        avgift = ''
        living_area = 0
        rooms = 0
        location = 0
        url = ''
        ref = ''

        item_id = item.div['data-item-id']
        for anchor in item.select('a.item-link-container'):
            if anchor.has_attr('href'):
                ref = anchor['href']

        item_body = item.find('div', class_='listing-post__details')

        if type(item_body).__name__!= 'NoneType':
            price = item_body.find('li', class_='price item-result-meta-attribute-is-bold').text.strip()
            avgift = item_body.find('li', class_='fee item-result-meta-attribute-subtle').text.strip()
            living_area = item_body.find('li', class_='living-area item-result-meta-attribute-is-bold').text.strip()
            rooms = item_body.find('li', class_='rooms item-result-meta-attribute-is-bold').text.strip()
            location = item_body.find('li', class_='city item-result-meta-attribute-subtle').text.strip()
            url = 'https://www.hemnet.se' + ref

        yield {
            'id': item_id,
            'price': re.sub('[^0-9]', '', price),
            'avgift': re.sub('[^0-9]', '', avgift),
            'area': living_area,
            'rooms': rooms,
            'location': location,
            'url': url
        }


def hemnet_fetch_all(location_id, page_limit=1, save_pages=True):
    """
    Fetch listings from Hemnet.se
    """
    print('Searching from Hemnet.se')

    browser = mechanicalsoup.StatefulBrowser()

    url = 'https://www.hemnet.se/bostader?region_id=' + location_id + '&item_types%5B%5D=bostadsratt'
    browser.open(url)
    page = browser.get_current_page()

    active = page.find('div', class_='pagination')
    next_page = active
    if active is not None:
        next_page = active.find('a', class_='next_page button button--primary')

    while next_page is not None and next_page.text.strip() == 'Nästa':
        print("***********************************")
        print("Page: ", next_page['href'])
        print("***********************************")
        for item in hemnet_fetch_items(page, save_pages):
            yield item
        
        time.sleep(random.randint(3, 10))  # Random sleep

        browser.open('https://www.hemnet.se' + next_page['href'])
        page = browser.get_current_page()
        next_page = page.find('div', class_='pagination') \
                        .find('a', class_='next_page button button--primary')
