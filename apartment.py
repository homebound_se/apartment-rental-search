"""
Define an apartment.
"""


class Apartment(object):
    def __init__(self):
        self._price = 0
        self._location = ''
        self._description = ''
        self._url = ''

    @property
    def description(self):
        return self._description

    @property
    def location(self):
        return self._location

    @property
    def price(self):
        return self._price

    @property
    def url(self):
        return self._url

